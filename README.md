# Summary 

This package can be used to activate primay replica connection in a 
MariaDB/MySQL environment


**Installation**
Prerequisites : 
	Pimcore v10.x.x


Once the bundle is installed , we will need to update the database.yaml file 

In the database configuration file you will need to add the wrapper class and add replica's configuration 

```bash
doctrine:
    dbal:
        connections:
            default:
                wrapper_class: WebSolutions\MasterSlaveBundle\Db\Connection,
                replicas:
                    replica1:
                          host: 'replica1'
                          port: 3306
                          dbname: dbname
                          user: username
                          password: password
                          charset: UTF8MB4
                    replica2:
                          host: 'replica2'
                          port: 3306
                          dbname: dbname
                          user: username
                          password: password
                          charset: UTF8MB4

```


Note : wrapper_class must be same as shown above
