<?php

namespace WebSolutions\MasterSlaveBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class WebSolutionsMasterSlaveBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/websolutionsmasterslave/js/pimcore/startup.js'
        ];
    }
}