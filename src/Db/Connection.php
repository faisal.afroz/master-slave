<?php

namespace WebSolutions\MasterSlaveBundle\Db;

use Doctrine\DBAL\Cache\QueryCacheProfile;
use Pimcore\Db\PimcoreExtensionsTrait;
use Pimcore\Db\ConnectionInterface;

class Connection extends \Doctrine\DBAL\Connections\PrimaryReadReplicaConnection implements ConnectionInterface
{
    use PimcoreExtensionsTrait;

    public function connect($connectionName = null)
    {
        $returnValue = parent::connect($connectionName);

        if ($returnValue) {
            $this->_conn->query('SET default_storage_engine=InnoDB;');
            $this->_conn->query("SET sql_mode = '';");
        }

        return $returnValue;
    }


    /**
     * Gets the parameters used during instantiation.
     * Overriden : Keeping the primary database data info at root level
     * because Pimcore Adminer reads it from the root level
     *
     * @internal
     *
     * @return array<string,mixed>
     * @psalm-return Params
     * @phpstan-return array<string,mixed>
     */
    public function getParams()
    {
        $params =  parent::getParams();
        $params['host'] = $params['primary']['host'] ?? null;
        $params['port'] = $params['primary']['port'] ?? null;
        $params['user'] = $params['primary']['user'] ?? null;
        $params['password'] = $params['primary']['password'] ?? null;

        $params['dbname'] = $params['primary']['dbname'] ?? null;
        return $params;
    }

    function executeQuery($query, array $params = [], $types = [], ?QueryCacheProfile $qcp = null)
    {
        $this->switchConnection($query);
        return parent::executeQuery($query, $params, $types, $qcp);
    }

    function executeStatement($sql, array $params = [], array $types = [])
    {
        $this->switchConnection($sql);

        return parent::executeStatement($sql, $params, $types);
    }

    protected function switchConnection($query)
    {

        // if $query is a insert/update/delete statement then switch to primary connection
        if (preg_match('/^\s*(insert|update|delete)/i', $query)) {
            $this->ensureConnectedToPrimary();
            // \Pimcore\Log\Simple::log("primary-replica.log","primary : " . $query);
        }
    }
}
