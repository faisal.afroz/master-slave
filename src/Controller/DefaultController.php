<?php

namespace WebSolutions\MasterSlaveBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends FrontendController
{
    /**
     * @Route("/web_solutions_master_slave")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello world from web_solutions_master_slave');
    }
}
