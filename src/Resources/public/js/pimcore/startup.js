pimcore.registerNS("pimcore.plugin.WebSolutionsMasterSlaveBundle");

pimcore.plugin.WebSolutionsMasterSlaveBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.WebSolutionsMasterSlaveBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("WebSolutionsMasterSlaveBundle ready!");
    }
});

var WebSolutionsMasterSlaveBundlePlugin = new pimcore.plugin.WebSolutionsMasterSlaveBundle();
